# 03 - Learning about conditionals!

# The standered if statement
a = 11
if a == 11
	puts "a is infact 11"
end #=> "a is infaxt 11"

# Some if/else magic!
a = 136534
if a != 314
	puts false
else
	puts true
end #=> false

# Else if!
a = 1
if a == 2
	puts "a is 2"
elsif a == 1
	puts "a is 1"
end #=> "a is 1"

# ==: value is equal to
# ===: value *and* type are equal to
# !=: value is not
# !==: value and type is not
# ||: or
# &&: and