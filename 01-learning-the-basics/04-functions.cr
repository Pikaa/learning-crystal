# 04 - Functions!!
# It's time for functions, hopefully this is fun!

# Let's create a simple function, using the techniques we've learned before!

# Define a function named "what_is_life" with a param named "guess" that has to be a number
def what_is_life(guess : Number)
	# Check if guess is 42
	if guess == 42
		puts "You guessed correctly! The meaning of life, according to Google, is indeed 42!"
	else
		puts "Try again, kiddo!"
	end
end

# Let's call our new function!
what_is_life(41) #=> "Try again, kiddo!"
what_is_life(42) #=> "You guessed correctly! The meaning of life, according to Google, is indeed 42!"