# 02 - Learning about the various types of Crystal!

# So... we actually got some code to run! Sweet!
# Now, let's see the different types we can use in Crystal!

# String - incased in quotes
string = "Woah, a string!"

# Strings can also be formatted, like this!
word = "listen!"
formatted_string = "Hey, #{word}"
puts formatted_string #=> Hey, listen!


# Ints - ezpz
int = 101


# Floats!
float = 95.5 # PLJ!


# Arrays - here is where the real fun starts!
array = [1, 2, 3, 4, 5]

# Arrays can contain as many different types as you'd like!
diff_types_array = [1, 2, 3, 'a', 'b', 'c']


# A tuple!
tuple = {1, 2, 3}


# A named tuple! Pretty similar to JSOn!
named_tuple = {hey: "listen!", isnt: "crystal", cool: true}
# And they are accessed in almost the same way as Python!
puts named_tuple[:hey] #=> listen!