# 01 - A simple "Hello, world!" program

# We don't need a "main" function, that *is* the program!
# "puts" = "print"
puts "Hello, world!"